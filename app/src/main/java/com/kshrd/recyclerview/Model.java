package com.kshrd.recyclerview;

public class Model {

    private String name;
    private int image;
    private String des;

    public Model(String name, int image, String des) {
        this.name = name;
        this.image = image;
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return "Model{" +
                "name='" + name + '\'' +
                ", image=" + image +
                ", des='" + des + '\'' +
                '}';
    }

}
