package com.kshrd.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Custom extends RecyclerView.Adapter<Custom.ViewHolder> {

    private Context context;
    private List<Model> modelList;

    public Custom(Context context, List<Model> modelList){
        this.context = context;
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public Custom.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutInflater = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(layoutInflater);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Custom.ViewHolder holder, int position) {
        Model model = modelList.get(position);
        holder.name.setText(model.getName());
        holder.image.setImageResource(model.getImage());
        holder.des.setText(model.getDes());
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView name,des;
        public ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_name);
            des = itemView.findViewById(R.id.txt_des);
            image = itemView.findViewById(R.id.imageView);

        }
    }

}
